# ArgoCD



## Getting started
### run these commands in git bash 
```
 $ helm pull argo/argo-cd --version 5.41.0 -d "C:\Users\yovel\Desktop" --untar
    $ change port 8080 > 
    $ helm install my-argo-cd .\argo-cd\ -n cicd
    $ kubectl -n cicd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
    $ kubectl port-forward service/my-argo-cd-argocd-server -n cicd 8080:443
```

* go to localhost:8080
connect with username: admin and the password you recieved 

## ArgoCD config 

part 1 - add repository 
go to Setting -> Repositories -> + Connect repo -> Choose your connection method: {in this case: via HTTPS} -> change type to HELM -> give your repo connection a name and a project name -> insert repository Url {in this case: oci://registry-1.docker.io/yovelchen} -> at the buttom click Enable OCI -> at the top left click connect. 

part 2 - create application 
go to Setting -> Repositories -> find your repository and click the three dots -> create application -> insert application name and project name -> add your chart name and version -> add destination -> click the directory button and change it to HELM -> than find your values.yaml and click it to be added -> than insert the parameters that needs to be changed -> click connect -> click sync 

